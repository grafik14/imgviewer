import os, sys, getopt
from Tkinter import *

global canvas

class AutoScrollbar(Scrollbar):
    # a scrollbar that hides itself if it's not needed.  only
    # works if you use the grid geometry manager.
    def set(self, lo, hi):
        if float(lo) <= 0.0 and float(hi) >= 1.0:
            # grid_remove is currently missing from Tkinter!
            self.tk.call("grid", "remove", self)
        else:
            self.grid()
        Scrollbar.set(self, lo, hi)
        
    def pack(self, **kw):
        raise TclError, "cannot use pack with this widget"
    
    def place(self, **kw):
        raise TclError, "cannot use place with this widget"


def on_clic(event):
    canvas = event.widget
    x = canvas.canvasx(event.x)
    y = canvas.canvasy(event.y)
    print x, y
    file = open("imgviewer.log","w") 
    file.write(str(int(x))+"\n") 
    file.write(str(int(y))+"\n") 
    file.close()
    exit(0)


def mouse_wheel_handler(event):
    canvas = event.widget
    # respond to Linux or Windows wheel event
    if event.num == 5 or event.delta == -120:
        canvas.yview_scroll(2,"units")
    if event.num == 4 or event.delta == 120:
        canvas.yview_scroll(-2,"units")
        

def key_home(event):
    global canvas
    canvas.yview_moveto(0.0)
        

def key_end(event):
    global canvas
    canvas.yview_moveto(1.0)
        


    
#
# create scrolled canvas

if __name__ == '__main__':

    global canvas

    inputfile = ''
    outputfile = ''
    try:
        opts, args = getopt.getopt(sys.argv[1:],"hi:o:",["input=","output="])
    except getopt.GetoptError:
        print 'imgviewer.py -i <inputfile> -o <outputfile>'
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print 'imgviewer.py -i <inputfile> -o <outputfile>'
            sys.exit()
        elif opt in ("-i", "--input"):
            inputfile = arg
        elif opt in ("-o", "--output"):
            outputfile = arg
    print 'Input file is ', inputfile
    print 'Output file is ', outputfile
       
    root = Tk()

    root.bind("<MouseWheel>",mouse_wheel_handler)
    root.bind("<Button-4>", mouse_wheel_handler)
    root.bind("<Button-5>", mouse_wheel_handler)

    root.bind('<Home>', key_home)
    root.bind('<End>', key_end)

    vscrollbar = AutoScrollbar(root)
    vscrollbar.grid(row=0, column=1, sticky=N+S)
    hscrollbar = AutoScrollbar(root, orient=HORIZONTAL)
    hscrollbar.grid(row=1, column=0, sticky=E+W)

    # For test
    #photo = PhotoImage(file="tmp-folder-pdf-signe/page_1.png")
    photo = PhotoImage(file=inputfile)

    canvas = Canvas(root,width=1000, height=800, 
                    yscrollcommand=vscrollbar.set,
                    xscrollcommand=hscrollbar.set)
    canvas.grid(row=0, column=0, sticky=N+S+E+W)
    canvas.bind("<Button-1>", on_clic)

    vscrollbar.config(command=canvas.yview)
    hscrollbar.config(command=canvas.xview)

    # make the canvas expandable
    root.grid_rowconfigure(0, weight=1)
    root.grid_columnconfigure(0, weight=1)

    canvas.create_image(0, 0, anchor=NW, image=photo)

    canvas.config(scrollregion=canvas.bbox("all"))

    root.mainloop()
